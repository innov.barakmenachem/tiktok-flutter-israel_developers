import 'package:flutter/material.dart';
import 'package:flutter_tiktok_clone/pages/video_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(),
      home: PageView(
        children: [
          VideoPage(
            videoPath: 'lib/assets/videos/video_1_tiktok.mp4',
          ),
          VideoPage(
            videoPath: 'lib/assets/videos/video_2_tiktok.mp4',
          ),
          VideoPage(
            videoPath: 'lib/assets/videos/video_3_tiktok.mp4',
          ),
        ],
        scrollDirection: Axis.vertical,
      ),
    );
  }
}
