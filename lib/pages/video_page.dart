import 'package:flutter/material.dart';
import 'package:flutter_tiktok_clone/widgets/video_tiktok_player.dart';

class VideoPage extends StatelessWidget {
  final String videoPath;
  const VideoPage({Key key, this.videoPath}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Stack(
          children: [
            VideoPlayerScreen(
              videoPath: videoPath,
            ),
            FrontMenuBottons(),
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.black,
          items: [
            BottomNavigationBarItem(
              icon: Icon(
                Icons.add,
                color: Colors.white,
              ),
              label: "add",
            ),
            BottomNavigationBarItem(
              icon: Image(
                height: 30,
                image: AssetImage('lib/assets/images/add_tiktok.png'),
              ),
              label: "",
            ),
            BottomNavigationBarItem(
              label: "profile",
              icon: Icon(
                Icons.person,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class FrontMenuBottons extends StatelessWidget {
  const FrontMenuBottons({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        UpperMenu(),
        VideoMenu(),
      ],
    );
  }
}

class VideoMenu extends StatelessWidget {
  const VideoMenu({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Expanded(
            child: VideoDescription(),
          ),
          SocialBottons(),
        ],
      ),
    );
  }
}

class SocialBottons extends StatelessWidget {
  const SocialBottons({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: 100,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            SocialBottonWithLable(
              icon: Icons.comment,
              label: "comment",
            ),
            SocialBottonWithLable(
              icon: Icons.favorite,
              label: "like",
            ),
            SocialBottonWithLable(
              icon: Icons.share,
              label: "share",
            ),
          ],
        ),
      ),
    );
  }
}

class SocialBottonWithLable extends StatelessWidget {
  final String label;
  final IconData icon;
  const SocialBottonWithLable({
    Key key,
    this.label,
    this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        IconButton(
          icon: Icon(
            icon,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text(
          label,
          style: TextStyle(color: Colors.white),
        ),
      ],
    );
  }
}

class VideoDescription extends StatelessWidget {
  const VideoDescription({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: MediaQuery.of(context).size.height * 0.1,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "@UserName",
              style: TextStyle(color: Colors.white),
            ),
            Text(
              "This is the Description of the Video",
              style: TextStyle(color: Colors.white),
            ),
            Text(
              "This is the song",
              style: TextStyle(color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }
}

class UpperMenu extends StatelessWidget {
  const UpperMenu({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 75,
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () {},
              child: Text(
                "Following",
                style: TextStyle(color: Colors.white),
              ),
            ),
            SizedBox(
              width: 20,
            ),
            GestureDetector(
              onTap: () {},
              child: Text(
                "For You",
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
